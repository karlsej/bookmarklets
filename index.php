<?php
ob_start();
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

function getBookmarkletCode($f) {
    
    ob_start();
    include_once($f);
    $string = ob_get_clean();
    return $string;
}
$data = array(
              array(
                'title' => 'Find ebooks matching items in bookstore website',
                'label' => 'match ebooks',
                'description' => '<p>This allows you to quickly determine whether Los Rios holds an ebook matching an assigned text from the bookstore. It collects all ISBNs shown on  a bookstore\'s page, queries the EDS API, and displays results on the page. <a href="https://drive.google.com/file/d/1jnNSFx5OV96mh5ISXN_YQQeakOxa1fC7/view?usp=sharing">View demo</a>.</p>',
                'script' => getBookmarkletCode('bookstore-ebook-check/bookmarklet.php')
                
              ),
              array(
                'title' => 'Add links to Gobi screens',
                'label' => 'Gobi links',
                'description' => '<p>Adds links to the OPAC and OneSearch to check for presence of titles listed on a Gobi screen (including ebooks &amp; different editions). Display is a little odd&mdash;links display twice in each table cell&mdash;because of poor Gobi markup.</p>',
                'script' => getBookmarkletCode('gobi-links/bookmarklet.php')
                
                
              )
              
              
              );
$markup = '';
$toc = '<ul id="toc">';
for ($i = 0; $i < count($data); $i++) {
    $toc .= '<li><a href="#bookmarklets-' . ($i + 1) . '">' . $data[$i]["title"] . '</a></li>';
        $markup .= '<h2 id="bookmarklets-' . ($i + 1) . '">' . $data[$i]["title"] . '</h2>';
        $markup .= '<div class="description">' . $data[$i]["description"] . '</div>';
        $markup .='<p>Drag the  bookmarklet to your browser bar or otherwise add it: ';
  //      $markup .= $data[$i]['script'];
        $markup .= '<a class="bookmarklet" href="javascript:' . ($data[$i]["script"]) . '">' . $data[$i]["label"] . '</a>';
        $markup .= '</p>';
        

    
}
$toc .= '<ul>';
ob_end_flush();
?>

<!DOCTYPE html>

<html lang="eng-US">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <title>Bookmarklets for SCC/Los Rios</title>
    <link href="style.css" rel="stylesheet" type="text/css">
</head>

<body>
    <div id="container">
        <h1>Bookmarklets</h1>
        
    <?php
    
 // links are not yet needed
 //   echo $toc;
    echo $markup;
   
  ?>
    </div>


</body>
</html>