(function() {

	window.gobiStuff = {};
	gobiStuff.placeLinks = function(el, data) {
		var linkBox = el.find('.linksBoxRight');
		var opacRoot = 'https://lasiii.losrios.edu';
		var edsRoot = 'https://ezproxy.losrios.edu/login?url=https://search.ebscohost.com/login.aspx?authtype=ip&amp;groupid=main&amp;profile=eds&amp;direct=true&amp;site=eds-live&amp;scope-site&amp;bquery=';
		var author = '';
		if (data.Author) {
			author = data.Author;
		} else if (data.Editor) {
			author = data.Editor;
		}
		var ebscoIB = '';
		if (data.ISBN) {
			ebscoIB = '+OR+IB+' + data.ISBN;
		}
		var links = [{
				url: opacRoot + '/search~/i?SEARCH=' + data.ISBN,
				text: 'search OPAC by ISBN'
			},
			{
				url: edsRoot + '&quot;' + data.Title + '&quot;+' + author + '+AND+PT+BOOK' + ebscoIB,
				text: 'search EDS by keyword/ISBN'
			}

		];

		for (var i = 0; i < links.length; i++) {

			linkBox.append('<div><a class="LinkLook bookmarklet-added" target="_blank" href="' + links[i].url + '">' + links[i].text + '</a></div>');


		}

	};


	gobiStuff.metadata = [];
	gobiStuff.getData = function(el, fieldType) {
		//console.log(dataType);

		var field = el.next();
		if (field.length) {
			var data = field.html();
			if ((fieldType === 'Author' || fieldType === 'Editor')) {

				var arr = data.split(',');
				data = arr[0];
			}

			return data;

		}
	};

	gobiStuff.init = function() {
		gobiStuff.records = $('.itemTable');
		if (!(gobiStuff.records.find('.bookmarklet-added').length)) {
		gobiStuff.records.each(function() { // collect metadata in object, put links on side
			var a = $(this);

			var itemData = {};
			var labels = a.find('.label');
			var fields = ['ISBN', 'Title', 'Author', 'Editor', 'Pub Year'];
			labels.each(function() {
				var b = $(this);
				for (var i = 0; i < fields.length; i++) {

					if (b.html().indexOf(fields[i]) === 0) {

						//		console.log(fields[i] + ': ' + getData(b, fields[i]));
						itemData[fields[i]] = gobiStuff.getData(b, fields[i]);



					}


				}



			});
			//console.log(itemData);
			gobiStuff.metadata.push(itemData);
			gobiStuff.placeLinks(a, itemData);



		});
		}
	};
	
	gobiStuff.init();

}());