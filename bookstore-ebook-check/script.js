function checkPager(el) {
	if ($('.pager').length) {
		el.after('<p id="pager-check">This list may have multiple pages. Be sure to check them all.</p>');
	}

}

function summaryFocus() {
	var a = $('.ebookcheck-summary');
	a.attr('tabindex', '0');
	a.focus();
	a.blur();
}

function bookCheckInit() {
	var bookstoreRegex = /books(tore)?\.(ar|cr|fl|sc)c\.losrios\.edu/;
	if (bookstoreRegex.test(location.hostname) !== false) {
		var count = 0;
		$('#eds-error').remove();
		$('.isbn .hidden').remove(); // apparent anti-scraping measure? There is a hidden element inserting the string 'bogus' into the ISBN
		var usedISBNs = []; // save work by putting ISBNs into array and making sure the one being searched has not already been
		$('.isbn').each(function() {
			var ib = $(this).text();
			ib = ib.trim();
			// only put each isbn in once
			if (usedISBNs.indexOf(ib) === -1) {
				usedISBNs.push(ib);
				count++;
			}

		});
		console.log(usedISBNs);
		if (usedISBNs.length === 0) {
			$('.main_content').prepend('<p id="no-ebooks" class="ebookcheck-summary">No ISBNs on this page&mdash;nothing to check.</p>');
			checkPager($('#no-ebooks'));
			summaryFocus();
		} else {


			var query = '(IB+' + usedISBNs.join('+OR+IB+') + ')+AND+PT+ebook';
			console.log(query);
			// search EBSCO for ebook	

			jQuery.getJSON('https://widgets.ebscohost.com/prod/encryptedkey/eds/eds.php?k=eyJjdCI6Ikc0d3VwcWxVTE1FS2sxXC9GVExvblhnPT0iLCJpdiI6Ijk1ZDIwNTUxZDdlMTU0OTliNzhkNGUxYWU0MDk1MjBlIiwicyI6IjdkY2QwMDcyNzNlYTYzMjMifQ==&p=c2FjcmFtLm1haW4ud3NhcGk=&s=0,1,1,0,0,0&q=search?query=' + query + '&limiter=FT%3ay&&limiter=FT1%3an&sort=relevance&includefacets=n&searchmode=all&autosuggest=n&autocorrect=n&view=brief&resultsperpage=20&pagenumber=1&highlight=n')
				.done(function(data) {
					console.log(data);
					window.data = data;

					if (data.SearchResult.Statistics.TotalHits > 0) {
						console.log('hits');
						var ebIBs = [];

						// build array of ISBNs
						var records = data.SearchResult.Data.Records;
						for (var i = 0; i < records.length; i++) {

							var ids = records[i].RecordInfo.BibRecord.BibRelationships.IsPartOfRelationships[0].BibEntity.Identifiers;
							for (var j = 0; j < ids.length; j++) {
								if (ids[j].Type.indexOf('isbn') > -1) {
									ebIBs.push(ids[j].Value);
								}
							}
						}
						console.log(ebIBs);

						// so now we have an array containing all those IBs.
						// go back through the ones on the page and do what we do.

						$('.isbn').each(function() {
							var el = $(this);
							var ib = el.text().trim();
							if (ebIBs.indexOf(ib) > -1) {
								el.closest('.material_info').prepend('<div class="found"><a href="https://ezproxy.losrios.edu/login?url=https://search.ebscohost.com/login.aspx?authtype=ip&groupid=main&profile=eds&direct=true&bquery=IB+' + ib + '+AND+PT+ebook&site=eds-live&scope=site" target="_blank">&#x1f44d; Possible match!</a></div>');
							} else {
								el.closest('.material_info').prepend('<div class="not-found">not found</div>');
							}

						});

						//			
					} else {
						var plural = '';
						if (count > 1) {
							plural = 's';
						}
						$('.main_content').prepend('<p id="no-ebooks" class="ebookcheck-summary">&#x1f44e; ' + count +' unique ISBN' + plural +' searched. No ebooks found for items shown on this page.</p>');
						checkPager($('#no-ebooks'));
						summaryFocus();

					}

					if ($('.found').length) {
						$('.main_content').prepend('<p id="possible-matches" class="ebookcheck-summary">&#x1f44d; Possible matches on this page!</p>');
						checkPager($('#possible-matches'));
						summaryFocus();
					}

				})
				.fail(function() {
					$('.main_content').prepend('<p class="ebookcheck-summary" id="eds-error">Something went wrong. <button type="button" onclick="bookCheckInit();">Try this again.</p>');

				});


		}
	} else {
		alert('This script only runs on Los Rios bookstore sites.');
	}
}
bookCheckInit();